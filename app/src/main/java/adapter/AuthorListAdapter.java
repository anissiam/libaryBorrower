package adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import geek.libaryborrower.anis.libaryborrower.AdapterCallback;
import geek.libaryborrower.anis.libaryborrower.CustomAddAuthorDialog;
import geek.libaryborrower.anis.libaryborrower.R;
import model.Author;

public class AuthorListAdapter extends RecyclerView.Adapter<AuthorListAdapter.ViewHolder>{
    Context context;
    ArrayList<Author> List_Author;
    private AdapterCallback mAdapterCallback;
    Activity activity;
    private CustomAddAuthorDialog customAddAuthorDialog;
    final private ListItemClickListener mOnClickListener;
    public AuthorListAdapter(Context context, ArrayList<Author> list_Author ,ListItemClickListener listener) {
        this.context = context;
        List_Author = list_Author;
        try {
            this.mAdapterCallback = ((AdapterCallback) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }

        mOnClickListener = listener;
    }
    public interface ListItemClickListener {
        void onListItemClick();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_author, viewGroup, false);
        AuthorListAdapter.ViewHolder holder = new AuthorListAdapter.ViewHolder(row);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Author item = List_Author.get(i);
        final ImageView authorImage = viewHolder.imgAuthor;
        Glide.with(context).load(item.getImage()).into(authorImage);
        viewHolder.tvAuthorName.setText(item.getName());
        viewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mAdapterCallback.onButtonClick(item.getName());
                mAdapterCallback.addAuthorId(item.getId());
                mOnClickListener.onListItemClick();

            }
        });

    }

    @Override
    public int getItemCount() {
        return List_Author.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        ImageView imgAuthor;
        TextView tvAuthorName;
        Button btnAdd;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgAuthor = (ImageView) itemView.findViewById(R.id.imgAuthor);
            tvAuthorName = (TextView) itemView.findViewById(R.id.tvAuthorName);
            btnAdd = (Button) itemView.findViewById(R.id.btnAdd);
        }
    }

}
