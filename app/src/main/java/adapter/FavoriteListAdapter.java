package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import geek.libaryborrower.anis.libaryborrower.R;
import model.Books;

public class FavoriteListAdapter extends RecyclerView.Adapter<FavoriteListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Books> bookList ;

    public FavoriteListAdapter(Context context, ArrayList<Books> bookList) {
        this.context = context;
        this.bookList = bookList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_home_favorite, viewGroup, false);
        FavoriteListAdapter.ViewHolder holder = new FavoriteListAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Books item = bookList.get(i);

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgBookCover;
        private TextView tvBookTitle;
        private TextView tvAuthorName;
        private RatingBar ratingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgBookCover = (ImageView) itemView.findViewById(R.id.imgBookCover);
            tvBookTitle = (TextView) itemView.findViewById(R.id.tvBookTitle);
            tvAuthorName = (TextView) itemView.findViewById(R.id.tvAuthorName);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
        }

    }
}