package adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import geek.libaryborrower.anis.libaryborrower.BottomNavigationActivity;
import geek.libaryborrower.anis.libaryborrower.ProfileBookActivity;
import geek.libaryborrower.anis.libaryborrower.R;
import geek.libaryborrower.anis.libaryborrower.SplashActivity;
import jp.wasabeef.blurry.Blurry;
import model.Books;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class HomeNewBookAdapter extends RecyclerView.Adapter<HomeNewBookAdapter.ViewHolder> {
    Context context;
    ArrayList<Books> List_Books;
    private FirebaseFirestore mFirestore;
    public HomeNewBookAdapter(Context context, ArrayList<Books> list_Books) {
        this.context = context;
        List_Books = list_Books;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_home, parent, false);
        ViewHolder holder = new ViewHolder(row);
        mFirestore =  FirebaseFirestore.getInstance();
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Books item = List_Books.get(i);
        // viewHolder.imgBookCover.setImageDrawable(item.setCover());
        viewHolder.tvBookTitle.setText(item.getTitle());
        int size = item.getAuthors().size();

        Log.d("Listt size from adapter", size + "");
        /*List<String> list = new ArrayList<>();
        list.add(item.getAuthors().toString());
        for (int l = 0; l < list.size(); l++) {
            for (int n = 0; n < list.get(l).length(); n++) {
                mFirestore.collection("Author").document(list.get(n))
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    String name = documentSnapshot.getString("name");
                                    viewHolder.tvBookAuther.setText(name);
                                    Log.d("listt ", name);
                                }
                            }
                        });
            }
        }*/



        for (final String author : item.getAuthors()) {
                mFirestore.collection("Author").document(author)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    String name = documentSnapshot.getString("name");
                                    viewHolder.tvBookAuther.setText(name+"\n");
                                    Log.d("listttttt ", name+"-->Book ID: "+ item.getId());
                                }
                            }
                        });

        }


       /* for (final String author : item.getAuthors()) {
            mFirestore.collection("Author").document(author)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                String name = documentSnapshot.getString("name");
                                viewHolder.tvBookAuther.setText(name);
                            }
                        }
                    });
        }*/

        /*for(int ii=0;ii<size;ii++)
        {
            viewHolder.tvBookAuther.setText(item.getAuthors().get(ii)+ "\n");
            Log.d("Listtt from adapter  " ,item.getAuthors().get(ii) + "\n" );
            System.out.println(item.getAuthors().get(ii));

        }*/



        // Date Date_created = item.getDate_Created();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM / d / yyyy");
        //String date = dateFormat.format(Date_created);
        /* viewHolder.tvDateOfUpLode.setText(date);*/
        View view;
        ImageView userImageView = viewHolder.imgBookCover;
        Glide.with(context).load(List_Books.get(i).getCover())
                .apply(RequestOptions.bitmapTransform(new RoundedCorners(14)))
                .into(userImageView);

        //viewHolder.tvIsAvailable.setText(item.getAvailable());
        /* if (item.getAvailable()==true) {

            viewHolder.tvIsAvailable.setText("Available");
            viewHolder.tvIsAvailable.setBackgroundResource(R.color.available);
        } else {
            viewHolder.tvIsAvailable.setText("Not available");
            viewHolder.tvIsAvailable.setBackgroundResource(R.color.notAvailable);
        }
*/
        viewHolder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfileBookActivity.class);
                intent.putExtra("bookDocumentId", item.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List_Books.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgBookCover;
        private TextView tvBookTitle;
        private TextView tvBookAuther;
        /*private TextView tvDateOfUpLode;
        private TextView tvIsAvailable;
        private ImageView icon_like;*/
        private ConstraintLayout constraintLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgBookCover = (ImageView) itemView.findViewById(R.id.imgBookCover);
            tvBookTitle = (TextView) itemView.findViewById(R.id.tvBookTitle);
            tvBookAuther = (TextView) itemView.findViewById(R.id.tvBookAuther);
           /* tvDateOfUpLode = (TextView)itemView.findViewById(R.id.tvDateOfUpLode);
            tvIsAvailable =  (TextView)itemView.findViewById(R.id.tvIsAvailable);
            icon_like =  (ImageView) itemView.findViewById(R.id.icon_like);*/
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraintLayout);
        }
    }
}




