package adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import geek.libaryborrower.anis.libaryborrower.HomeFragment;
import geek.libaryborrower.anis.libaryborrower.NewBookListFragment;
import geek.libaryborrower.anis.libaryborrower.NotificationFragment;
import geek.libaryborrower.anis.libaryborrower.ProfileFragment;
import geek.libaryborrower.anis.libaryborrower.SearchFragment;

public class PagerViewAdapterHomeFragment extends FragmentPagerAdapter {
    public PagerViewAdapterHomeFragment(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0 :
                NewBookListFragment newBookListFragment = new NewBookListFragment();
                return newBookListFragment;
            case 1:

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }
}
