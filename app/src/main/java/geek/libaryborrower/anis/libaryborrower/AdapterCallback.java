package geek.libaryborrower.anis.libaryborrower;

public interface AdapterCallback {
    void onButtonClick(String name);
    void addAuthorId(String id);

}
