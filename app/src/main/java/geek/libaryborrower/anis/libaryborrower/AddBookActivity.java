package geek.libaryborrower.anis.libaryborrower;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import model.Author;

public class AddBookActivity extends AppCompatActivity implements AdapterCallback {
    private ImageView imgAddCover;
    private ImageView imgBookCover;
    private EditText edtTitle;
    private TextView tvAuthor;
    private Button btnAddAuthor;
    private EditText edtISBN;
    private EditText edtCategory;
    private TextView edtLanguage;
    private Button btnArabic;
    private Button btnEnglish;
    private EditText edtLinguisticEditor;
    private EditText edtCoverDesigner;
    private EditText edtPublisher;
    private EditText edtTranslator;
    private EditText edtDescription;
    private Button btnSave;
    private EditText edtEditionYear;
    private EditText edtEditionNo;
    private EditText edtPageNo;
    private EditText edtPlace;

    private Uri filePath;
    private static final int PICK_IMAGE_REQUEST = 234;
    private StorageReference storageReference;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    private String name;
    private ArrayList<String> authorID;
    private Author author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);
        imgAddCover = (ImageView) findViewById(R.id.imgAddCover);
        imgBookCover = (ImageView) findViewById(R.id.imgBookCover);
        edtTitle = (EditText) findViewById(R.id.edtTitle);
        tvAuthor = (TextView) findViewById(R.id.tvAuthor);
        btnAddAuthor = (Button) findViewById(R.id.btnAddAuthor);
        edtISBN = (EditText) findViewById(R.id.edtISBN);
        edtCategory = (EditText) findViewById(R.id.edtCategory);
        edtLanguage = (TextView) findViewById(R.id.edtLanguage);
        btnArabic = (Button) findViewById(R.id.btnArabic);
        btnEnglish = (Button) findViewById(R.id.btnEnglish);
        edtLinguisticEditor = (EditText) findViewById(R.id.edtLinguisticEditor);
        edtCoverDesigner = (EditText) findViewById(R.id.edtCoverDesigner);
        edtPublisher = (EditText) findViewById(R.id.edtPublisher);
        edtTranslator = (EditText) findViewById(R.id.edtTranslator);
        edtDescription = (EditText) findViewById(R.id.edtDescription);
        btnSave = (Button) findViewById(R.id.btnSave);
        edtEditionYear = (EditText) findViewById(R.id.edtEditionYear);
        edtEditionNo = (EditText) findViewById(R.id.edtEditionNo);
        edtPageNo = (EditText) findViewById(R.id.edtPageNo);
        edtPlace = (EditText) findViewById(R.id.edtPlace);

        storageReference = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();

        author = new Author();
        authorID = new ArrayList<>();
        imgAddCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upLoadFile();
            }
        });

        btnArabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnArabic.setBackgroundResource(R.color.pink);
                btnEnglish.setBackgroundResource(R.color.gray);

            }
        });
        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnEnglish.setBackgroundResource(R.color.pink);
                btnArabic.setBackgroundResource(R.color.gray);

            }
        });
        btnAddAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomAddAuthorDialog customAddAuthorDialog = new CustomAddAuthorDialog();
                customAddAuthorDialog.show(getSupportFragmentManager(), "CustomAddAuthorDialog");
            }
        });
    }

    private void upLoadFile() {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            StorageReference riversRef = storageReference.child("Images/BookCover/" + edtTitle.getText().toString() + ".jpg");
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String download_url = taskSnapshot.getTask().getResult().getDownloadUrl().toString();
                            String title = edtTitle.getText().toString();
                            String ISBN = edtISBN.getText().toString();
                            String category = edtCategory.getText().toString();
                            String linguisticEditor = edtLinguisticEditor.getText().toString();
                            String coverDesigner = edtCoverDesigner.getText().toString();
                            String publisher = edtPublisher.getText().toString();
                            String translator = edtTranslator.getText().toString();
                            String description = edtDescription.getText().toString();
                            String editionYear = edtEditionYear.getText().toString();
                            String editionNo = edtEditionNo.getText().toString();
                            String pageNo = edtPageNo.getText().toString();
                            String place = edtPlace.getText().toString();

                            String language = null;
                            if (btnArabic.isClickable()) {
                                language = btnArabic.getText().toString();
                            } else if (btnEnglish.isClickable()) {
                                language = btnEnglish.getText().toString();
                            } else {
                                Toast.makeText(AddBookActivity.this, "select language",
                                        Toast.LENGTH_SHORT).show();
                            }

                            boolean available = true;
                            Date date_of_uplode = new Date();
                            Map<String, Object> bookMap = new HashMap<>();
                            bookMap.put("title", title);
                            bookMap.put("image", download_url);
                            bookMap.put("ISBN", ISBN);
                            bookMap.put("category", category);
                            bookMap.put("linguisticEditor", linguisticEditor);
                            bookMap.put("coverDesigner", coverDesigner);
                            bookMap.put("publisher", publisher);
                            bookMap.put("translator", translator);
                            bookMap.put("description", description);
                            bookMap.put("editionYear", editionYear);
                            bookMap.put("editionNo", editionNo);
                            bookMap.put("pageNo", pageNo);
                            bookMap.put("place", place);
                            bookMap.put("language", language);
                            bookMap.put("available", available);
                            bookMap.put("date_of_uplode", date_of_uplode);
                            bookMap.put("authorId", authorID);
                            mFireStore.collection("Books").document()
                                    .set(bookMap)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            progressDialog.dismiss();
                                            Toast.makeText(AddBookActivity.this, "File uploading", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(AddBookActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage(((int) progress) + "% uploading...");
                }
            });
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent, "Select an Image"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap
                        (getContentResolver(), filePath);
                imgBookCover.setImageBitmap(bitmap);
            } catch (IOException e) {

            }
        }
    }

    @Override
    public void onButtonClick(String name) {
        String text = tvAuthor.getText().toString();
        if (!text.isEmpty()) {
            tvAuthor.setText(text + " , " + name);
        } else {
            tvAuthor.setText(name);
        }
    }

    @Override
    public void addAuthorId(String id) {
        authorID.add(id);
    }
}

