package geek.libaryborrower.anis.libaryborrower;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class AddNewAuthorDialogFragment extends DialogFragment {
    //widget
    private ImageView imgAuthorPic;
    private ImageView imgAddCover;
    private EditText edtEmail;
    private EditText edtName;
    static private TextView edtBirthDate;
    private EditText edtFacebook;
    private EditText edtTwitter;
    private EditText edtGoodReader;
    private Button btnSave;
    //var
    private static final int PICK_IMAGE_REQUEST=234;
    private Uri filePath;
    private StorageReference storageReference;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    private AdapterCallback mAdapterCallback;
    static private Calendar calendar;
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_new_author_dialoge, container, false);
        imgAuthorPic = (ImageView) view.findViewById(R.id.imgAuthorPic);
        imgAddCover = (ImageView) view.findViewById(R.id.imgAddCover);
        edtEmail = (EditText)view.findViewById(R.id.edtEmail);
        edtName = (EditText)view.findViewById(R.id.edtName);
        edtBirthDate = (TextView) view.findViewById(R.id.edtBirthDate);
        edtFacebook = (EditText)view.findViewById(R.id.edtFacebook);
        edtTwitter = (EditText)view.findViewById(R.id.edtTwitter);
        edtGoodReader = (EditText)view.findViewById(R.id.edtGoodReader);
        btnSave = (Button) view.findViewById(R.id.btnSave);
    //from web ide
        storageReference = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        imgAddCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
        edtBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                newFragment.show(fragmentManager, "datePicker");
            }
        });
        try {
            this.mAdapterCallback = ((AdapterCallback) getActivity());
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewAuthor();
            }
        });

        return view;

    }

    private void addNewAuthor() {
        if(filePath !=null) {
            final ProgressDialog progressDialog= new ProgressDialog(getContext());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            StorageReference riversRef = storageReference.child("Images/AuthorPic/"+edtName.getText().toString()+".jpg");
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                            String download_url = taskSnapshot.getTask().getResult().getDownloadUrl().toString();
                            final String name = edtName.getText().toString();
                            String email = edtEmail.getText().toString();
                            String facebook = edtFacebook.getText().toString();
                            String twitter = edtTwitter.getText().toString();
                            String goodReader = edtGoodReader.getText().toString();
                            String birthDate = edtBirthDate.getText().toString();
                            Log.d("Calender", calendar.getTime()+"");
                            Map<String, Object> authorMap = new HashMap<>();
                            authorMap.put("name", name);
                            authorMap.put("image", download_url);
                            authorMap.put("email", email);
                            authorMap.put("facebook", facebook);
                            authorMap.put("twitter", twitter);
                            authorMap.put("goodReader", goodReader);
                            authorMap.put("birthDate", calendar.getTime());
                            mAdapterCallback.onButtonClick(name);
                            final DocumentReference documentReference = mFireStore.collection("Author").document();
                            documentReference
                                    .set(authorMap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            progressDialog.dismiss();
                                            documentReference.getId();

                                            Log.d("documentReference", documentReference.getId());
                                            mAdapterCallback.addAuthorId(documentReference.getId());
                                            getDialog().dismiss();
                                            Toast.makeText(getContext(), "uploading ...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage(((int)progress)+"% uploading...");
                }
            });
        }
    }

    private void showFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"Select an Image"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode==PICK_IMAGE_REQUEST &&resultCode==RESULT_OK &&
                data!=null && data.getData()!=null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap
                        (getContext().getContentResolver(), filePath);
                imgAuthorPic.setImageBitmap(bitmap);
            } catch (IOException e) {

            }
        }
    }



    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getContext(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar = Calendar.getInstance();
            calendar.set(year,monthOfYear,dayOfMonth);
            edtBirthDate.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);

        }
    }
}
