package geek.libaryborrower.anis.libaryborrower;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddUserDialoge extends DialogFragment {

    private Button btnLibrarian;
    private Button btnBorrower;


    public AddUserDialoge() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_user_dialoge, container, false);
        btnLibrarian = (Button) view.findViewById(R.id.btnLibrarian);
        btnBorrower = (Button) view.findViewById(R.id.btnBorrower);
        btnLibrarian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent UserDataIntent = new Intent(getContext(),LibrarianDataActivity.class);
                startActivity(UserDataIntent);
                getDialog().dismiss();
            }
        });

        btnBorrower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent UserDataIntent = new Intent(getContext(),BorrwerDataActivity.class);
                startActivity(UserDataIntent);
                getDialog().dismiss();
            }
        });
        return view;

    }

}
