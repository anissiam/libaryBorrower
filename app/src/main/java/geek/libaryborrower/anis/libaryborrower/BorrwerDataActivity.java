package geek.libaryborrower.anis.libaryborrower;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class BorrwerDataActivity extends AppCompatActivity {
    private CircleImageView imageBorrower;
    private EditText edtName;
    private EditText edtPhone;
    private EditText edtEmail;
    private Button btnAddPhoto;
    private static TextView edtBirthDate;
    private Button btnSave;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mfirestore;
    private StorageReference storageReference;
    private static final int PICK_IMAGE_REQUEST=234;
    private Uri filePath;
    static private Calendar calendar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrwer_data);

        imageBorrower = (CircleImageView)findViewById(R.id.imageBorrower);
        edtName = (EditText)findViewById(R.id.edtName);
        edtPhone = (EditText)findViewById(R.id.edtPhone);
        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtBirthDate = (TextView)findViewById(R.id.edtBirthDate);
        btnSave= (Button)findViewById(R.id.btnSave);
        btnAddPhoto = (Button) findViewById(R.id.btnAddPhoto);
        mAuth = FirebaseAuth.getInstance();
        mfirestore = FirebaseFirestore.getInstance();
        progressDialog= new ProgressDialog(this);
        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
        storageReference= FirebaseStorage.getInstance().getReference();

        filePath = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl() ;
        if (filePath!=null) {
            Glide.with(BorrwerDataActivity.this).load(filePath).into(imageBorrower);
        }

        edtBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        final String user_id = mAuth.getCurrentUser().getUid();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upLode();
            }
        });

    }
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"Select an Image"),PICK_IMAGE_REQUEST);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode==PICK_IMAGE_REQUEST &&resultCode==RESULT_OK &&
                data!=null && data.getData()!=null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap
                        (getContentResolver(), filePath);
                imageBorrower.setImageBitmap(bitmap);

            } catch (IOException e) {

            }
        }
    }
    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar = Calendar.getInstance();
            calendar.set(year,monthOfYear,dayOfMonth);

            edtBirthDate.setText(year +"/"+(monthOfYear+1)+"/"+dayOfMonth);
        }

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        if (progressDialog !=null && progressDialog.isShowing() ){
            progressDialog.cancel();
        }
    }
    public void upLode(){
        final String user_id = mAuth.getCurrentUser().getUid();
        if(filePath !=null) {

            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            StorageReference riversRef = storageReference.child("Images/Users/Borrower/"+edtName.getText().toString()+".jpg");
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            String download_url = taskSnapshot.getTask().getResult().getDownloadUrl().toString();
                            String name = edtName.getText().toString().trim();
                            String phone = edtPhone.getText().toString().trim();
                            String email = edtEmail.getText().toString().trim();
                            //-- if user =false --> borrower
                            boolean user = false;
                            ///--change BirthDate to Date member
                            String birthDate = edtBirthDate.getText().toString().trim();

                            //String token_id = FirebaseInstanceId.getInstance().getToken();
                            Map<String, Object> userMap = new HashMap<>();
                            userMap.put("name", name);
                            userMap.put("phone", phone);
                            userMap.put("email", email);
                            userMap.put("birthDate", calendar.getTime());
                            userMap.put("user", user);
                            userMap.put("authId", user_id);
                            userMap.put("image",download_url);

                            mfirestore.collection("Users").document(user_id)
                                    .set(userMap)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Intent HomeIntent = new Intent(BorrwerDataActivity.this,BottomNavigationActivity.class);
                                            startActivity(HomeIntent);
                                            finish();
                                            //Toast.makeText(BorrwerDataActivity.this, "success", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(BorrwerDataActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage(((int)progress)+"% uploading...");
                }
            });
        }
    }
}
