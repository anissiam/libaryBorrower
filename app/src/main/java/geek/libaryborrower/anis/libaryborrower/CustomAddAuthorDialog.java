package geek.libaryborrower.anis.libaryborrower;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;

import adapter.AuthorListAdapter;
import model.Author;

import static android.content.Context.MODE_PRIVATE;

public class CustomAddAuthorDialog extends DialogFragment  implements AuthorListAdapter.ListItemClickListener{

    //widget
    private EditText edtSearch;
    private Button btnCancel;
    private Button btnSearch;
    private RecyclerView recyclerView;
    private LinearLayout linAddAuthor;
    private Button btnAddAuthor;
    //var
    private ArrayList<Author> List_Author;
    private AuthorListAdapter authorListAdapter;
    private Author author;
    private FirebaseFirestore mFirestore;
    private String name ;
    private String id ;
    private ProgressDialog dialog;
    private ConstraintLayout container;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_author_dialoge, container, false);

        edtSearch = (EditText) view.findViewById(R.id.edtSearch);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnSearch = (Button) view.findViewById(R.id.btnSearch);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        linAddAuthor = (LinearLayout) view.findViewById(R.id.linAddAuthor);
        btnAddAuthor = (Button) view.findViewById(R.id.btnAddAuthor);
        container = (ConstraintLayout) view.findViewById(R.id.container);
        //var declration
        List_Author = new ArrayList<>();
        authorListAdapter = new AuthorListAdapter(getContext(), List_Author,this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(authorListAdapter);
        mFirestore = FirebaseFirestore.getInstance();
        dialog = new ProgressDialog(getContext());
        name = edtSearch.getText().toString();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchAuthor();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        btnAddAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dismiss();

                AddNewAuthorDialogFragment frag = new AddNewAuthorDialogFragment();
                frag.show(getActivity().getSupportFragmentManager(),"AddNewAuthorDialogFragment");
                getDialog().dismiss();
                /*FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();*/
            }
        });
        return view;
    }

    private void searchAuthor() {
        List_Author.clear();
        name = edtSearch.getText().toString();
        mFirestore.collection("Author").whereEqualTo("name", name)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        //Log.d("anis1", task.getException()+"qq");
//                        if(task.getException()==null){
//
//                        }
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                recyclerView.setVisibility(View.INVISIBLE);
                                linAddAuthor.setVisibility(View.VISIBLE);
                            } else {
                                for (DocumentSnapshot document : task.getResult()) {
                                    Log.d("anis1", document.getId() + " => " + document.getData());

                                    id = document.getId();
                                    Log.d("anis1", "id" + id);


                                    String id = document.getId();
                                    String name = document.getString("name");
                                    String email = document.getString("email");
                                    Date birthDate = document.getDate("birthDate");
                                    String image = document.getString("image");
                                    /*SharedPreferences.Editor editor = getActivity().getSharedPreferences(Appkey.getAppkey(), MODE_PRIVATE).edit();
                                    editor.putString("authorName",name);
                                    editor.putString("authorID",id);
                                    editor.apply();*/
                                    author = document.toObject(Author.class);
                                    author.setId(id);
                                    author.setName(name);
                                    author.setEmail(email);
                                    author.setBirthDate(birthDate);
                                    author.setImage(image);
                                    List_Author.add(author);
                                    authorListAdapter.notifyDataSetChanged();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    linAddAuthor.setVisibility(View.INVISIBLE);

                                }
                            }
                        } else {
                            Log.d("anis1", "id"+id);
                            Log.d("anis1", "Error getting documents1: ", task.getException());
                        }
                    }
                });

      /*  if (id != null) {
            mFirestore.collection("Author").document(id)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            dialog.dismiss();
                            Log.d("anis2", documentSnapshot.getId() + " => " + documentSnapshot.getData());


                        }
                    });
        }else {
            Log.d("anis2", "Doc not found ");

        }*/
       /* mFirestore.collection("Author").whereEqualTo("name",name)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if (e != null) {

                            Toast.makeText(getContext(), "Error ", Toast.LENGTH_SHORT).show();
                            *//*recyclerView.setVisibility(View.INVISIBLE);
                            linAddAuthor.setVisibility(View.VISIBLE);*//*
                            return;
                        }

                        for (DocumentChange document : documentSnapshots.getDocumentChanges()) {
                            if (document.getType() == DocumentChange.Type.ADDED ) {

                                String id = document.getDocument().getId();
                                String name = document.getDocument().getString("name");
                                String email = document.getDocument().getString("email");
                                Date birthDate = document.getDocument().getDate("birthDate");
                                String image = document.getDocument().getString("image");

                                author = document.getDocument().toObject(Author.class);
                                author.setId(id);
                                author.setName(name);
                                author.setEmail(email);
                                author.setBirthDate(birthDate);
                                author.setImage(image);
                                List_Author.add(author);
                                authorListAdapter.notifyDataSetChanged();
                                Toast.makeText(getContext(), "aa", Toast.LENGTH_SHORT).show();
                            } else {
                                //document.getDocument();
                                recyclerView.setVisibility(View.INVISIBLE);
                                linAddAuthor.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                });
*/    }

    @Override
    public void onListItemClick() {
        getDialog().dismiss();
    }
}

