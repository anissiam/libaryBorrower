package geek.libaryborrower.anis.libaryborrower;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditBorrowerDataDialog extends DialogFragment {
    private CircleImageView imageBorrower;
    private EditText edtName;
    private EditText edtPhone;
    private EditText edtEmail;
    private Button btnAddPhoto;
    private Button btnCancel;
    private static TextView edtBirthDate;
    private Button btnSave;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mfirestore;
    private StorageReference storageReference;
    private static final int PICK_IMAGE_REQUEST=234;
    private Uri filePath;
    static private Calendar calendar;
    private ProgressDialog progressDialog;
    private String id ,name ,email,phone,image;
    private Date birthDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_borrower_data_dialoge, container, false);
        imageBorrower = (CircleImageView)view.findViewById(R.id.imageBorrower);
        edtName = (EditText)view.findViewById(R.id.edtName);
        edtPhone = (EditText)view.findViewById(R.id.edtPhone);
        edtEmail = (EditText)view.findViewById(R.id.edtEmail);
        edtBirthDate = (TextView)view.findViewById(R.id.edtBirthDate);
        btnSave= (Button)view.findViewById(R.id.btnSave);
        btnAddPhoto = (Button)view.findViewById(R.id.btnAddPhoto);
        btnCancel= (Button)view.findViewById(R.id.btnCancel) ;

        mAuth = FirebaseAuth.getInstance();
        mfirestore = FirebaseFirestore.getInstance();
        progressDialog= new ProgressDialog(getContext());

        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        storageReference= FirebaseStorage.getInstance().getReference();
        edtBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        getBorrowerData();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upDateBorrowerData();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        return view;
    }

    private void upDateBorrowerData() {

    }

    private void getBorrowerData() {
        final String user_id = mAuth.getCurrentUser().getUid();
        mfirestore.collection("Users").document(user_id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if(documentSnapshot.exists()){
                                name = documentSnapshot.getString("name");
                                edtName.setText(name);

                                id = documentSnapshot.getId();

                                email = documentSnapshot.getString("email");
                                edtEmail.setText(email);

                                phone = documentSnapshot.getString("phone");
                                edtPhone.setText(phone);

                                image = documentSnapshot.getString("image");
                                Picasso.get().load(image).into(imageBorrower);

                                birthDate = documentSnapshot.getDate("birthDate");
                                SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy");
                                String birthDateS = timeStampFormat.format(birthDate);
                                edtBirthDate.setText(birthDateS);

                            }else {
                                Log.d("documentSnapshot", "error :no user with id "+ user_id+" is exist");
                            }
                        }else {
                            Log.d("task", "error : "+ task.getException());
                        }
                    }
                });
    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"Select an Image"),PICK_IMAGE_REQUEST);
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("RECEIVED!!");

        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode==PICK_IMAGE_REQUEST &&resultCode==RESULT_OK &&
                data!=null && data.getData()!=null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap
                        (getContext().getContentResolver(), filePath);
                imageBorrower.setImageBitmap(bitmap);
            } catch (IOException e) {

            }
        }*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                    data != null && data.getData() != null) {
                filePath = data.getData();
                Log.d("WTFF0", filePath + "");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap
                            (getActivity().getContentResolver(), filePath);
                    imageBorrower.setImageBitmap(bitmap);
                    Log.d("WTFF1", "error");
                    progressDialog.setTitle("Uploading...");
                    progressDialog.show();
                    StorageReference riversRef = storageReference.child("Images/Users/Borrower/" + id + ".jpg");
                    riversRef.putFile(filePath)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    progressDialog.dismiss();
                                    String download_url = taskSnapshot.getTask().getResult().getDownloadUrl().toString();
                                    mfirestore.collection("Users").document(mAuth.getUid())
                                            .update("image", download_url)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    progressDialog.dismiss();
                                                }
                                            });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "Error : Try again", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage(((int) progress) + "% uploading...");
                            if (progress == 100.0) {
                                Thread closeActivity = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(1000);
                                            progressDialog.setTitle("Picture is changed");
                                            // Do some stuff
                                        } catch (Exception e) {
                                            e.getLocalizedMessage();
                                        }
                                    }
                                });
                            }
                        }
                    });
                } catch (IOException e) {
                    Log.d("Error:", e.getMessage());
                }
            }
            //super.onActivityResult(requestCode, resultCode, data);
        }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar = Calendar.getInstance();
            calendar.set(year,monthOfYear,dayOfMonth);

            edtBirthDate.setText(year +"/"+(monthOfYear+1)+"/"+dayOfMonth);
        }

    }
}
