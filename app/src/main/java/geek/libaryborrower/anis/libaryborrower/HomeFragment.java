package geek.libaryborrower.anis.libaryborrower;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class HomeFragment extends Fragment {
    private TextView tvNewBook ;
    private TextView tvFavorite ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        tvNewBook = (TextView) view.findViewById(R.id.tvNewBook);
        tvFavorite = (TextView) view.findViewById(R.id.tvFavorite);

        final NewBookListFragment newBookListFragment = new NewBookListFragment();
        final FavoriteFragment favoriteFragment = new FavoriteFragment();
        /*getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.containerFrame, newBookListFragment, "newBookListFragment")
                .addToBackStack(null)
                .commit();*/

        tvNewBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.containerFrame, newBookListFragment, "newBookListFragment")
                        .addToBackStack(null)
                        .commit();
                tvNewBook.setBackgroundResource(R.drawable.round_outline_pink);
                tvNewBook.setTextColor(Color.parseColor("#FFFFFF"));
                tvFavorite.setBackgroundResource(R.drawable.round_outline_gray);
                tvFavorite.setTextColor(Color.parseColor("#878787"));
            }
        });
        tvFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.containerFrame, favoriteFragment, "newBookListFragment")
                        .addToBackStack(null)
                        .commit();
                tvFavorite.setBackgroundResource(R.drawable.round_outline_pink);
                tvFavorite.setTextColor(Color.parseColor("#FFFFFF"));
                tvNewBook.setBackgroundResource(R.drawable.round_outline_gray);
                tvNewBook.setTextColor(Color.parseColor("#878787"));
            }
        });
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        final NewBookListFragment newBookListFragment = new NewBookListFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.containerFrame, newBookListFragment, "newBookListFragment")
                .addToBackStack(null)
                .commit();
    }

}