package geek.libaryborrower.anis.libaryborrower;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class LibrarianHomeActivity extends AppCompatActivity {
    private ImageView imgProfile;
    private ImageView imgAddBook;
    private ImageView imgBookBorrowed;
    private ImageView imgSearch;
    private Button btnProfile;
    private Button btnAddBook;
    private Button btnAddAuthor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_librarian_home);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgAddBook = (ImageView) findViewById(R.id.imgAddBook);
        imgBookBorrowed = (ImageView) findViewById(R.id.imgBookBorrowed);
        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        btnAddBook = (Button) findViewById(R.id.btnAddBook);
        btnAddAuthor = (Button) findViewById(R.id.btnAddAuthor);

        imgAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addBookIntent = new Intent(LibrarianHomeActivity.this,AddBookActivity.class);
                startActivity(addBookIntent);
            }
        });
        btnAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addBookIntent = new Intent(LibrarianHomeActivity.this,AddBookActivity.class);
                startActivity(addBookIntent);
            }
        });

        btnAddAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addAuthorBook = new Intent(LibrarianHomeActivity.this,AddAuthorActivity.class);
                startActivity(addAuthorBook);
            }
        });
    }
}
