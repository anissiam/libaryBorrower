package geek.libaryborrower.anis.libaryborrower;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "Google";
    private EditText edtEmail;
    private EditText edtPassword;
    private TextView tvForgetPassword;
    private RelativeLayout mGoogleSignInButton;
    private Button btnLogin;
    private TextView tvSignUp;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 1001;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        tvForgetPassword = (TextView) findViewById(R.id.txForgetPassword);
        mGoogleSignInButton = (RelativeLayout) findViewById(R.id.relative_google);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        tvForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUpIntent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(signUpIntent);
                finish();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
        mGoogleSignInButton.setOnClickListener(this);
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUpIntent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(signUpIntent);
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edtEmail.getText().toString().trim();
                String password = edtPassword.getText().toString();

                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
                    progressBar.setVisibility(View.VISIBLE);
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull final Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        SharedPreferences.Editor editor = getSharedPreferences(Appkey.getAppkey(), MODE_PRIVATE).edit();
                                        editor.putString("userID", mAuth.getUid());
                                        editor.apply();
                                        //Toast.makeText(LoginActivity.this, "success ", Toast.LENGTH_SHORT).show();
                                        /*Intent UserDataIntent = new Intent(LoginActivity.this,BorrwerDataActivity.class);
                                        startActivity(UserDataIntent);
                                        finish();*/
                                        progressBar.setVisibility(View.INVISIBLE);
                                        // User is signed in
                                        mFirestore.collection("Users").document(mAuth.getUid())
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            DocumentSnapshot document = task.getResult();
                                                            Log.d("hhhhh", document + "\n" + document.exists() + "\n" + document.getId());
                                                            if (document.exists()) {
                                                                boolean user = document.getBoolean("user");
                                                                if (user == true) {
                                                                    Intent UserDataIntent = new Intent(LoginActivity.this, LibrarianHomeActivity.class);
                                                                    startActivity(UserDataIntent);
                                                                    finish();
                                                                } else if (user == false) {
                                                                    Intent UserDataIntent = new Intent(LoginActivity.this, BottomNavigationActivity.class);
                                                                    startActivity(UserDataIntent);
                                                                    finish();
                                                                }
                                                            } else {
                                                                Snackbar.make(findViewById(R.id.container), "No Account SignUp please", Snackbar.LENGTH_LONG)
                                                                        .setAction("SignUp", new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View view) {
                                                                                Intent signUpIntent = new Intent(LoginActivity.this, SignupActivity.class);
                                                                                startActivity(signUpIntent);
                                                                                finish();
                                                                            }
                                                                        })
                                                                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                                                        .show();
                                                            }
                                                        }
                                                    }
                                                });
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Error :" + task.getException(), Toast.LENGTH_SHORT).show();
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                }
                            });
                }
            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    /*public Boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-])*@[A-Za-z0-9]+(\\.[A-Za-z0-9])*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }*/
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGooogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            SharedPreferences.Editor editor = getSharedPreferences(Appkey.getAppkey(), MODE_PRIVATE).edit();
                            editor.putString("userID", mAuth.getUid());
                            editor.apply();


                            mFirestore.collection("Users").document(mAuth.getUid())
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                DocumentSnapshot document = task.getResult();
                                                if (document.exists()) {
                                                    boolean userF = document.getBoolean("user");
                                                    if (document != null && document.exists()) {

                                                        if (userF == true) {
                                                            Intent UserDataIntent = new Intent(LoginActivity.this, LibrarianHomeActivity.class);
                                                            startActivity(UserDataIntent);
                                                            finish();
                                                        } else if (userF == false) {
                                                            Intent UserDataIntent = new Intent(LoginActivity.this, BottomNavigationActivity.class);
                                                            startActivity(UserDataIntent);
                                                            finish();
                                                        }
                                                    } /*else {

                                                    if (userF == true) {
                                                        Intent UserDataIntent = new Intent(LoginActivity.this, LibrarianDataActivity.class);
                                                        startActivity(UserDataIntent);
                                                        finish();
                                                    } else if (userF == false) {
                                                        Intent UserDataIntent = new Intent(LoginActivity.this, BorrwerDataActivity.class);
                                                        startActivity(UserDataIntent);
                                                        finish();
                                                    }
                                                }*/
                                                } else {
                                                    AddUserDialoge addUserDialoge = new AddUserDialoge();
                                                    addUserDialoge.show(getSupportFragmentManager(), "AddUserDialoge");
                                                }
                                            } else {
                                                String TAG = "Error";
                                                Log.d(TAG, "get failed with ", task.getException());
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed
                Log.e(TAG, "Google Sign In failed.");
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error." + connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_google:
                signIn();
                break;
            default:
                return;
        }

    }
}