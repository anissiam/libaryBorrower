package geek.libaryborrower.anis.libaryborrower;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;


import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;
import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyAccountFragment extends Fragment {
    private TextView tvEmail;
    private TextView tvPhone;
    private TextView tvBirthDate;
    private Button btnBack;
    private Button btnEdit;
    private FrameLayout containerFrame;
    private FirebaseFirestore firestore;

    public MyAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        tvEmail = (TextView) view.findViewById(R.id.tvEmail);
        tvPhone = (TextView) view.findViewById(R.id.tvPhone);
        tvBirthDate = (TextView) view.findViewById(R.id.tvBirthDate);
        btnEdit = (Button) view.findViewById(R.id.btnEdit);
        btnBack = (Button) view.findViewById(R.id.btnBack);
        containerFrame = (FrameLayout) view.findViewById(R.id.containerFrame);

        firestore = FirebaseFirestore.getInstance();
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditBorrowerDataDialog editBorrowerDataDialog = new EditBorrowerDataDialog();
                editBorrowerDataDialog.show(getActivity().getSupportFragmentManager(), "EditBorrowerDataDialog");
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileFragment profileFragment = new ProfileFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.containerFrame, profileFragment);
                transaction.commit();
            }
        });
        fetchUserData();
        return view;
    }

    private void fetchUserData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(Appkey.appkey, MODE_PRIVATE);
        String userID = prefs.getString("userID", "");
        firestore.collection("Users").document(userID).addSnapshotListener(getActivity(), new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "Error while loading!", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.toString());
                    return;
                }

                if (documentSnapshot.exists()) {
                    String email = documentSnapshot.getString("email");
                    tvEmail.setText(email);

                    String phone = documentSnapshot.getString("phone");
                    tvPhone.setText(phone);

                    Date birthDate = documentSnapshot.getDate("birthDate");
                    SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String birthDateS = timeStampFormat.format(birthDate);
                    tvBirthDate.setText(birthDateS);
                }
            }

        });
    }
}




                /*.get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        String email=documentSnapshot.getString("email");
                        tvEmail.setText(email);

                        String phone=documentSnapshot.getString("phone");
                        tvPhone.setText(phone);

                        String birthDate=documentSnapshot.getString("birthDate");
                        tvBirthDate.setText(birthDate);


                    }
                });*/



