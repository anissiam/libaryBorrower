package geek.libaryborrower.anis.libaryborrower;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import adapter.HomeNewBookAdapter;
import model.Books;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewBookListFragment extends Fragment {

    private RecyclerView recyclerView ;
    private ArrayList<Books> List_Books;
    private HomeNewBookAdapter homeNewBookAdapter ;
    private Books books;
    private FirebaseFirestore mFirestore;
    private Activity mActivity;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvFavorites;
    private boolean isFoundAutor;
    private boolean isFoundBook;
    public NewBookListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_book_list, container, false);
        mFirestore = FirebaseFirestore.getInstance();
        /*tvFavorites = (TextView) view.findViewById(R.id.tvFavorites);*/
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        List_Books = new ArrayList<>();
        homeNewBookAdapter = new HomeNewBookAdapter(container.getContext(), List_Books);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(container.getContext(),LinearLayoutManager.VERTICAL
                /*LinearLayoutManager.HORIZONTAL,*/ ,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(homeNewBookAdapter);
        getBookData();

        SharedPreferences prefs = getActivity().getSharedPreferences(Appkey.appkey, MODE_PRIVATE);
        String userID = prefs.getString("userID", "");
        /*QuerySnapshot collectionReferenceFavoriteAuthor = mFirestore.collection("Users").document(userID)
                .collection("FavoriteAuthor").get().addOnSuccessListener();
        CollectionReference collectionReferenceFavoriteBook = mFirestore.collection("Users").document("TZ7p9cGv8CbAT1twnWVO7eaWZ8b2")
                .collection("FavoriteBook");

        if (collectionReferenceFavoriteAuthor!=null || collectionReferenceFavoriteBook!=null){
            tvFavorites.setVisibility(View.VISIBLE);
        }

        Log.d("FavoritB", collectionReferenceFavoriteBook+"");
        Log.d("FavoritA", collectionReferenceFavoriteAuthor+"");*/

        /*isCollectionFavoriteAuthorExist();*/
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getNewBookData();
                getBookData();

            }
        });

        return view;
    }
    public void getBookData(){
        //List_Books.clear();
        mFirestore.collection("Books").orderBy("date_of_uplode",Query.Direction.DESCENDING)
                .addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        for (DocumentChange doc :documentSnapshots.getDocumentChanges()){
                            if(doc.getType()==DocumentChange.Type.ADDED){
                                books = doc.getDocument().toObject(Books.class);
                                String id = doc.getDocument().getId();
                                String cover_Image = doc.getDocument().getString("image");
                                String title = doc.getDocument().getString("title");

                                List<String> list = (List<String>) doc.getDocument().get("authorId");
                                for(String a : list){
                                    books.setAuthors(list);
                                }

                        /*ArrayList<String> list1 = new ArrayList<>();
                        //list.add( author);
                        list.add(list1);*/

                                Log.d("Listtt from  firestore" ,doc.getDocument().get("authorId")+"" );
                                Log.d("Listtt from  array list" ,list+"" );
                                Log.d("Listtt size" ,list.size()+"" );
                                //String[] authors = (String[]) list.get(0);

                                String ISBN = doc.getDocument().getString("ISBN");
                                //boolean available = doc.getDocument().getBoolean("available");
                                //Date date_of_uplode =  doc.getDocument().getDate("date_of_uplode ");
                                //Log.d("TAGG",doc.getDocument().getDate("date_of_uplode ")+"");


                                books.setCover(cover_Image);
                                books.setTitle(title);
                        /*for (int i = 0 ; i<books.getAuthors().size();i++){
                            books.setAuthors((ArrayList<Object>) books.getAuthors().get(i));
                        }*/

                                books.setIsbn(ISBN);
                                books.setId(id);
                                //books.setAuthors(list);

                                for (String s : list){
                                    Log.d("MyBooks",s);}

                                //books.setAvailable(available);
                                //books.setDate_Created(date_of_uplode);
                                List_Books.add(books);
                                homeNewBookAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    public void getNewBookData(){
        List_Books.clear();
        mFirestore.collection("Books").orderBy("date_of_uplode",Query.Direction.DESCENDING)
                .addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        for (DocumentChange doc :documentSnapshots.getDocumentChanges()){
                            if(doc.getType()==DocumentChange.Type.ADDED){
                                swipeRefreshLayout.setRefreshing(false);
                                String user_id = doc.getDocument().getId();
                                String cover_Image = doc.getDocument().getString("image");
                                String title = doc.getDocument().getString("title");
                                String ISBN = doc.getDocument().getString("ISBN");
                                List list = (List<String>) doc.getDocument().get("authorId");
                                //String author = doc.getDocument().getString("author");
                                //boolean available = doc.getDocument().getBoolean("available");
                                //Date date_of_uplode =  doc.getDocument().getDate("date_of_uplode ");
                                //Log.d("TAGG",doc.getDocument().getDate("date_of_uplode ")+"");

                                books = doc.getDocument().toObject(Books.class);
                                books.setCover(cover_Image);
                                books.setTitle(title);
                                books.setIsbn(ISBN);
                                books.setAuthors(list);
                        /*for (int i = 0 ; i<books.getAuthors().size();i++){
                            books.setAuthors((ArrayList<Object>) books.getAuthors().get(i));
                        }*/
                                //books.setAvailable(available);
                                //books.setDate_Created(date_of_uplode);
                                List_Books.add(books);
                                homeNewBookAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
   /* private void isCollectionFavoriteAuthorExist() {
        SharedPreferences prefs = getActivity().getSharedPreferences(Appkey.appkey, MODE_PRIVATE);
        String userID = prefs.getString("userID", "");
        //TZ7p9cGv8CbAT1twnWVO7eaWZ8b2
        *//*mFirestore.collection("Users").document(userID).collection("FavoriteAuthor")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot documentSnapshots) {
                        if (!documentSnapshots.isEmpty()) {
                            Log.d("isFound", documentSnapshots.isEmpty() + "");
                            isFoundAutor = documentSnapshots.isEmpty();
                            tvFavorites.setVisibility(View.VISIBLE);
                        }else if (documentSnapshots.isEmpty()){
                            isFoundAutor = documentSnapshots.isEmpty();
                            tvFavorites.setVisibility(View.INVISIBLE);
                            Log.d("isFound", documentSnapshots.isEmpty() + "");
                        }
                    }
                });*//*
        mFirestore.collection("Users").document(userID).collection("FavoriteBook")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot documentSnapshots) {
                        if (!documentSnapshots.isEmpty()) {
                            Log.d("isFound", documentSnapshots.isEmpty() + "");
                            isFoundBook = documentSnapshots.isEmpty();
                            tvFavorites.setVisibility(View.VISIBLE);
                            for(DocumentChange documentSnapshot :documentSnapshots.getDocumentChanges()){
                                String id = documentSnapshot.getDocument().getId();
                                Log.d("recevidd", id+"1");
                                mFirestore.collection("Books").document(id)
                                        .get()
                                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                if (documentSnapshot.exists()) {
                                                    Books books = documentSnapshot.toObject(Books.class);
                                                    String cover_Image = documentSnapshot.getString("image");
                                                    books.setCover(cover_Image);
                                                    String title = documentSnapshot.getString("title");
                                                    books.setTitle(title);
                                                    List list = (List<String>) documentSnapshot.get("authorId");
                                                    books.setAuthors(list);
                                                    List_Books.add(books);
                                                }else {

                                                }
                                            }
                                        });
                            }
                        } else if (documentSnapshots.isEmpty()) {
                            isFoundBook = documentSnapshots.isEmpty();
                            tvFavorites.setVisibility(View.INVISIBLE);
                            Log.d("isFound", documentSnapshots.isEmpty() + "");
                        }
                    }
                });
    }*/
}
