package geek.libaryborrower.anis.libaryborrower;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.jgabrielfreitas.core.BlurImageView;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import jp.wasabeef.glide.transformations.BlurTransformation;
import model.Books;

import static android.support.constraint.Constraints.TAG;
import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
import static java.util.Collections.copy;


public class ProfileBookActivity extends AppCompatActivity {
    private static final int IO_BUFFER_SIZE = 1000;
    private ImageView imgBookCover;
    private TextView tvType;
    private TextView tvTitle;
    private TextView tvBookAuthor;
    private TextView tvISBN;
    private TextView tvEdition;
    private TextView tvNoOfPage;
    private TextView tvPublisher;
    private TextView tvLinguisticEditor;
    private TextView tvCoverDesigner;
    private TextView tvCategory;
    private TextView tvLanguage;
    private TextView tvIsAvailable;
    private TextView tvDescription;
    private ImageView bImageView;
    private TextView tvPlace;
    private Context context;
    private FirebaseFirestore firestore ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_book);
        imgBookCover = (ImageView) findViewById(R.id.imgBookCover);
        tvType = (TextView) findViewById(R.id.tvType);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvBookAuthor = (TextView) findViewById(R.id.tvBookAuthor);
        tvISBN = (TextView) findViewById(R.id.tvISBN);
        tvEdition = (TextView) findViewById(R.id.tvEdition);
        tvNoOfPage = (TextView) findViewById(R.id.tvNoOfPage);
        tvPublisher = (TextView) findViewById(R.id.tvPublisher);
        tvLinguisticEditor = (TextView) findViewById(R.id.tvLinguisticEditor);
        tvCoverDesigner = (TextView) findViewById(R.id.tvCoverDesigner);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvLanguage = (TextView) findViewById(R.id.tvLanguage);
        tvIsAvailable = (TextView) findViewById(R.id.tvAvailable);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        bImageView =(ImageView) findViewById(R.id.bImageView);
        tvPlace = (TextView)findViewById(R.id.tvPlace) ;
        firestore =  FirebaseFirestore.getInstance();
        fetchAllBookData();

    }

    private void fetchAllBookData() {
        Intent i= getIntent();
        String bookDocumentId= i.getStringExtra("bookDocumentId");

        firestore.collection("Books").document(bookDocumentId)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                        if (e != null) {

                            Toast.makeText(ProfileBookActivity.this , "Error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, e.toString());
                            return;
                        }
                        if (documentSnapshot != null && documentSnapshot.exists()) {
                            String Title = documentSnapshot.getString("title");
                            tvTitle.setText(Title);
                            String cover = documentSnapshot.getString("image");
                            Glide.with(getApplicationContext())
                                    .load(cover)
                                    .apply(bitmapTransform(new BlurTransformation(30)))
                                    .into(bImageView);
                            Picasso.get().load(cover).into(imgBookCover);

                            String Description = documentSnapshot.getString("description");
                            tvDescription.setText(Description);

                            //String author = documentSnapshot.getString("author");

                            /*Books books =  documentSnapshot.toObject(Books.class);
                            books.getAuthors();
                            for(int i =0 ; i<books.getAuthors().size();i++){
                                tvBookAuthor.setText(books.getAuthors().get(i).toString());
                            }*/
                            List<String> authors = (List<String>) documentSnapshot.get("authorId");
                            for (String author :authors) {
                                firestore.collection("Author").document(author)
                                        .get()
                                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                if (documentSnapshot.exists()) {
                                                    String name = documentSnapshot.getString("name");
                                                    tvBookAuthor.setText(name);
                                                }
                                            }
                                        });
                            }
                            boolean available = documentSnapshot.getBoolean("available");
                            if (available == true) {
                                tvIsAvailable.setText("available");
                                tvIsAvailable.setBackgroundResource(R.color.available);
                            } else {
                                tvIsAvailable.setText("Not available");
                                tvIsAvailable.setBackgroundResource(R.color.notAvailable);
                            }

                            String isbn = documentSnapshot.getString("ISBN");
                            tvISBN.setText("ISBN: " + isbn);

                            String category = documentSnapshot.getString("category");
                            tvCategory.setText(category);

                            String editionYear = documentSnapshot.getString("editionYear");
                            String edition = documentSnapshot.getString("edition");
                            //ordinal(Integer.parseInt(edition));
                            //  tvEdition.setText(ordinal(Integer.parseInt(edition)) + "Edition at " + editionYear);

                            String language = documentSnapshot.getString("language");
                            tvLanguage.setText(language);

                            String linguisticEditor = documentSnapshot.getString("linguisticEditor");
                            tvLinguisticEditor.setText("Linguistic Editor: " + linguisticEditor);

                            String pageNumbers = documentSnapshot.getString("pageNo");
                            tvNoOfPage.setText(pageNumbers + " Pages");

                            String profileDesigner = documentSnapshot.getString("coverDesigner");
                            tvCoverDesigner.setText("Cover Designer: " + profileDesigner);

                            String publisher = documentSnapshot.getString("publisher");
                            tvPublisher.setText(publisher);

                            /*String translator =  document.getString("translator");
                               tv.setText(translator);*/

                            String type = documentSnapshot.getString("type");
                            tvType.setText(type);

                            String place =documentSnapshot.getString("place");
                            tvPlace.setText(place);
                        }
                    }
                });




                /*.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            String Title = document.getString("title");
                            tvTitle.setText(Title);

                            String cover = document.getString("cover");
                            Glide.with(getApplicationContext())
                                    .load(cover)
                                    .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                    .into(bImageView);
                            Picasso.get().load(cover).into(imgBookCover);

                            String Description = document.getString("description");
                            tvDescription.setText(Description);

                            String author = document.getString("author");
                            tvBookAuthor.setText(author);

                            boolean available = document.getBoolean("available");
                            if(available==true){
                                tvIsAvailable.setText("available");
                                tvIsAvailable.setBackgroundResource(R.color.available);
                            }else {
                                tvIsAvailable.setText("Not available");
                                tvIsAvailable.setBackgroundResource(R.color.notAvailable);
                            }

                            String isbn13 = document.getString("isbn13");
                            tvISBN.setText( "ISBN: "+isbn13);

                            String category =  document.getString("category");
                            tvCategory.setText(category);

                            String editionYear =document.getString("editionYear");
                            String edition =  document.getString("edition");
                            //ordinal(Integer.parseInt(edition));
                            tvEdition.setText(ordinal(Integer.parseInt(edition))+"Edition at "+editionYear);

                            String language =  document.getString("language");
                            tvLanguage.setText(language);

                            String linguisticEditor =  document.getString("linguisticEditor");
                            tvLinguisticEditor.setText("المدقق اللغوي : "+linguisticEditor);

                            String pageNumbers =  document.getString("pageNumbers");
                            tvNoOfPage.setText(pageNumbers+" Pages");

                            String profileDesigner =  document.getString("profileDesigner");
                            tvCoverDesigner.setText("مصمم الغلاف : "+profileDesigner);

                            String publisher =  document.getString("publisher");
                            tvPublisher.setText(publisher);

                            *//*String translator =  document.getString("translator");
                            tv.setText(translator);
*//*
                            String type =  document.getString("type");
                            tvType.setText(type);

                            double price = document.getDouble("price");
                            tvPrice.setText(price+" ");


                            }
                        }else {
                            Toast.makeText(ProfileBookActivity.this, "Error : "+task.getException() , Toast.LENGTH_SHORT).show();
                        }
                    }
                });*/

    }

    public static String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];

        }
    }


}
