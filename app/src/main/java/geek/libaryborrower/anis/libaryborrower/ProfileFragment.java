package geek.libaryborrower.anis.libaryborrower;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static android.support.constraint.Constraints.TAG;


public class ProfileFragment extends Fragment {
    private CircleImageView circleImageView;
    private TextView tvName;
    private FrameLayout containerFrame;
    private FirebaseFirestore firestore;
    private FirebaseAuth mAuth;
    private TextView tvLoading;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_profile, container, false);
        containerFrame = (FrameLayout) view.findViewById(R.id.containerFrame);
        /*tvMyAccount = (TextView) view.findViewById(R.id.tvMyAccount);
        tvBook = (TextView) view.findViewById(R.id.tvBook);
        tvBookInAway = (TextView) view.findViewById(R.id.tvBookInAway);
        tvLogout = (TextView) view.findViewById(R.id.tvLogout);
        btnBack = (Button) view.findViewById(R.id.btnBack);*/
        circleImageView = (CircleImageView) view.findViewById(R.id.circleImageView);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvLoading = (TextView) view.findViewById(R.id.tvLoading);

        firestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();





        /*btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*tvMyAccount.setVisibility(View.VISIBLE);
                tvBook.setVisibility(View.VISIBLE);
                tvBookInAway.setVisibility(View.VISIBLE);
                tvLogout.setVisibility(View.VISIBLE);
                btnBack.setVisibility(View.INVISIBLE);*//*
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.remove( myAccountFragment);
                transaction.commit();
            }
        });*/
/*
        tvMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*tvMyAccount.setVisibility(View.INVISIBLE);
                tvBook.setVisibility(View.INVISIBLE);
                tvBookInAway.setVisibility(View.INVISIBLE);
                tvLogout.setVisibility(View.INVISIBLE);
                btnBack.setVisibility(View.VISIBLE);*//*
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.profile_container, myAccountFragment);
                transaction.commit();
            }
        });*/
        ProfileListFragment profileListFragment = new ProfileListFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.containerFrame, profileListFragment);
        transaction.commit();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getUserName();
    }

    private void getUserName() {
        SharedPreferences prefs = getActivity().getSharedPreferences(Appkey.appkey, MODE_PRIVATE);
        String userID = prefs.getString("userID", "");
       /* if (FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl()!=null) {
            String url = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString();
            Picasso.get().load(url).into(circleImageView);
        }*/

        firestore.collection("Users").document(userID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                   @Override
                   public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                       if (e != null) {

                           Snackbar.make(getActivity().findViewById(R.id.container), "Try again Error ", Snackbar.LENGTH_LONG);
                           return;
                       }
                       if (documentSnapshot != null && documentSnapshot.exists()) {
                           Log.d(TAG, "Current data: " + documentSnapshot.getData());
                           String image = documentSnapshot.getString("image");
                           Picasso.get().load(image).into(circleImageView);
                           String name=documentSnapshot.getString("name");
                           tvName.setText(name);
                       } else {
                           Log.d(TAG, "Current data: null");
                       }
                   }
               });


/*
        firestore.collection("Users").document(userID)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        String image = documentSnapshot.getString("image");
                        Picasso.get().load(image).into(circleImageView);
                        String name=documentSnapshot.getString("name");
                        tvName.setText(name);
                    }
                });*/
    }

}
