package geek.libaryborrower.anis.libaryborrower;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;


public class ProfileListFragment extends Fragment {
    private TextView tvMyAccount,tvBook,tvBookInAway,tvLogout;
    private CircleImageView circleImageView;
    private TextView tvName;
    private FirebaseFirestore firestore;
    private FirebaseAuth mAuth;
    private FrameLayout containerFrame;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_list, container, false);
        containerFrame = (FrameLayout) view.findViewById(R.id.containerFrame);
        tvMyAccount = (TextView) view.findViewById(R.id.tvMyAccount);
        tvBook = (TextView) view.findViewById(R.id.tvBook);
        tvBookInAway = (TextView) view.findViewById(R.id.tvBookInAway);
        tvLogout = (TextView) view.findViewById(R.id.tvLogout);
        circleImageView = (CircleImageView) view.findViewById(R.id.circleImageView);
        tvName = (TextView) view.findViewById(R.id.tvName);

        final MyAccountFragment myAccountFragment = new MyAccountFragment();
        tvMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.containerFrame, myAccountFragment);
                transaction.commit();

            }
        });
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("SignOut")
                        .setMessage("Are you sure ")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                               /* auth = FirebaseAuth.getInstance();
                                auth.signOut();
                                finish();*/
                                FirebaseAuth.getInstance().signOut();
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.cancel();
                                //Toast.makeText(_cxt, "no", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });
        return view;
    }
}
