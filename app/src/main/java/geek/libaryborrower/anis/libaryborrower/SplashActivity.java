package geek.libaryborrower.anis.libaryborrower;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class SplashActivity extends AppCompatActivity {
    Thread splashTread;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mfirestore;
    private Boolean user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mAuth = FirebaseAuth.getInstance();
        mfirestore = FirebaseFirestore.getInstance();



        StartAnimations();

        }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.splash);
        iv.clearAnimation();
        iv.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 500) {
                        sleep(100);
                        waited += 100;
                    }

                } catch (InterruptedException e) {
                    // do nothing
                } finally {

                    if(mAuth.getUid()!=null){
                        mfirestore.collection("Users").document(mAuth.getUid())
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {
                                            user = documentSnapshot.getBoolean("user");
                                            FirebaseUser currentUser = mAuth.getCurrentUser();
                                            if (currentUser != null && user == true) {
                                                Intent intent = new Intent(SplashActivity.this, LibrarianHomeActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } else if (currentUser != null && user == false) {
                                                Intent intent = new Intent(SplashActivity.this, BottomNavigationActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }else {
                                            Intent intent = new Intent(SplashActivity.this,
                                                    LoginActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                            startActivity(intent);
                                            SplashActivity.this.finish();
                                        }
                                    }
                                });
                    }else if(mAuth.getUid()==null){
                        Intent intent = new Intent(SplashActivity.this,
                                LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        SplashActivity.this.finish();
                    }


                }
            }
        };
        splashTread.start();

    }
}
