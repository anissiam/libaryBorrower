package model;

import java.util.Date;

public class Author {
    String name;
    String image;
    String email;
    String phone;
    String id;
    Date birthDate;
    String [] books;

    public Author() {
    }

    public Author(String name, String image, String email, String phone, String id, Date birthDate, String[] books) {
        this.name = name;
        this.image = image;
        this.email = email;
        this.phone = phone;
        this.id = id;
        this.birthDate = birthDate;
        this.books = books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String[] getBooks() {
        return books;
    }

    public void setBooks(String[] books) {
        this.books = books;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
