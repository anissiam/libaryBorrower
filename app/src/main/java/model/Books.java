package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Books {
    String title , cover,isbn,barcode,edition,pageNumbers,publisher,translator,language
            ,linguisticEditor,description,profileDesigner,category,type,editionYear,id;
    Date date_Created;
    boolean available;
    double price;
     private List <String>Authors ;
//     Map<String, Object> author;
     /*private Object authorss;*/
    public Books() {
    }


    public Books(String title, String cover
            , String isbn, String barcode, String edition
            , String pageNumbers, String publisher, String translator
            , String language, String linguisticEditor, String description
            , String profileDesigner, boolean available, String category
            , String type, Date date_Created, String editionYear, double price, String id, List <String>authors) {
        this.title = title;

        this.cover = cover;
        this.isbn = isbn;
        this.barcode = barcode;
        this.edition = edition;
        this.pageNumbers = pageNumbers;
        this.publisher = publisher;
        this.translator = translator;
        this.language = language;
        this.linguisticEditor = linguisticEditor;
        this.description = description;
        this.profileDesigner = profileDesigner;
        this.available = available;
        this.category = category;
        this.type = type;
        this.date_Created = date_Created;
        this.editionYear=editionYear;
        this.price=price;
        this.id=id;
        Authors = authors;

    }

    /*public Map<String, Object> getAuthor() {
        return author;
    }

    public void setAuthor(Map<String, Object> author) {
        this.author = author;
    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List <String> getAuthors() {
        return Authors;
    }

    public void setAuthors(List <String> authors) {
        Authors = authors;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(String pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getTranslator() {
        return translator;
    }

    public void setTranslator(String translator) {
        this.translator = translator;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLinguisticEditor() {
        return linguisticEditor;
    }

    public void setLinguisticEditor(String linguisticEditor) {
        this.linguisticEditor = linguisticEditor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileDesigner() {
        return profileDesigner;
    }

    public void setProfileDesigner(String profileDesigner) {
        this.profileDesigner = profileDesigner;
    }

    public boolean getAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate_Created() {
        return date_Created;
    }

    public void setDate_Created(Date date_Created) {
        this.date_Created = date_Created;
    }

    public String getEditionYear() {
        return editionYear;
    }

    public void setEditionYear(String editionYear) {
        this.editionYear = editionYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
